package classifier.interfaces;

import java.io.IOException;

import classifiers.shared.LabelledTextList;

/**
 * 
 * @author Dania Bouhmidi
 * An object implemeneting IDataSetSerializer should provide a load() method
 * and a save method
 *
 */
public interface IDataSetSerializer {

	
	
	LabelledTextList load() throws IOException;
     void save(LabelledTextList labelledTextList) throws IOException;

}
