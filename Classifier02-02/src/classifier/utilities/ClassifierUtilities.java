package classifier.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dania Bouhmidi and Michelle Polonsky
 * @version 2019-11-23
 * ClassifierUtilities is used to arrange String arrays in order to classify them
 * It can remove punctuation and upper case letters from strings  
 * It can convert String arrays to strings,
 * It can remove adjacent duplicates from a String array
 */
public class ClassifierUtilities {

	/**
	 * Takes as input a string array, and returns it in lower case, without punctuation
	 * @param  a String array of words
	 * @return a String array with the same content in lower case, without punctuation
	 */ 
	public static String [] removePunctuationandCase(String array []){

		String [] newarray = new String [array.length];
		for (int i = 0; i<array.length; i++){
			newarray[i] = array[i].replaceAll("\\p{Punct}","").toLowerCase();
		}
		return newarray;
	}

	/**
	 * Takes a string array as input, removes its duplicates (if any) and it returns an array without duplicates
	 * @param a String array (that only contains one word per index)
	 * @return a String array without duplicate strings
	 */ 
	public static String[] removeDuplicates(String[] words){
		ArrayList<String> text = new ArrayList<String>();

		text.addAll(Arrays.asList(words));
		Set<String> noDuplicates = new HashSet<>(text);
		String[] removedDuplicates = new String[noDuplicates .size()];

		noDuplicates.toArray(removedDuplicates);
		return removedDuplicates;
	}


	/**
	 * Takes a string array as input and converts it to a String
	 * @param a String array
	 * @return all of the array indexes inside a String
	 */ 
	public static String  getArrayasString (String [] array) {
		StringBuilder sentences = new StringBuilder("");
		for (int i =0; i<array.length; i++){
			sentences = sentences.append(array[i]);
			if ( i != array.length-1){
				sentences = sentences.append(" ");
			}
		}

		return sentences.toString();
	}


	/**
	 * Sorts a String array alphabetically
	 * @param A String array of words
	 * @return void
	 */
	public static void alphabeticalSort(String [] words) {
		for(int i = 0; i < words.length-1; i++) {
			for (int j = i + 1; j <words.length ; j++) {
				if (words[i].compareTo(words[j]) > 0) {
					// swap words[i] with words[j[
					String temp = words[i];
					words[i] = words[j];
					words[j] = temp;
				}
			}
		}
	}
	/**
	 * Copies an Object array into another object array
	 * @param one Object[]  to put the copied values in
	 * @param two Object[] to be copied
	 */
	public static void copyArray (Object [] one, Object [] two) {
		for (int i =0; i<one.length; i++) {
			one[i] = two[i];
		}
	}
	
	/**
	 * Finds the index of the maximum value inside an array of integers
	 * @param counts array of integers
	 * @return index of the location of the integer with the maximum value
	 */
	public static int findMax(int [] counts) {
		int maxLocation = 0;
		int maxValue = counts[0];
		for (int i = 0; i < counts.length; i++ ){
			if (counts[i] > maxValue) {
				maxLocation = i;
				maxValue = counts[i];
			}
		}
		return maxLocation;
	}

	/**
	 * Takes as input two arrays and returns a String array containing both of them
	 * @param first String array to add to
	 * @param second String array to add
	 * @return a String array containing all the elements from both input arrays
	 */
	public static String [] addArrays(String[] first, String[] second) {
		ArrayList<String> list = new ArrayList<>();
		if(first != null) {
			for(String element: first){
				list.add(element);
			}
		}

		for(String element: second){
			list.add(element);
		}

		String[] both = list.toArray(new String[list.size()]);
		return both;
	}

	/**
	 * Binary searches for a word's occurence (by lexicographical order)
	 * It returns 0 if the word is found
	 * If the word is not found, it returns -1.
	 * @param a String to be compared 
	 * @param a String array to search in
	 * @return 0 (word found)
	 * @return -1  (word not found)
	 */ 
	public static  int find(String word,String []document ){

		int left = 0;
		int right = document.length-1;
		int middle = (left + right) / 2;
		while (left <= right) {
			if (document[middle].compareTo(word) < 0) {
				left = middle + 1;
			}  
			if (document[middle].compareTo(word)>0) {
				right = middle-1; 
			} 
			if (document[middle].compareTo(word)==0) {
				return 0;
			}
			middle = (left + right) / 2;
		}
		return -1;
	}

}
