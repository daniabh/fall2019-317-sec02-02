package classifiers.shared;

import classifiers.shared.CategorizerBase;

import java.util.Arrays;

import classifier.utilities.ClassifierUtilities;
/**
 * @author Dania Bouhmidi
 * SingleCategorizer serves as a categorizer for LabelledTextList lists that contain multiple categories
 * SingleCategorizer enables us to determine the most frequent of those categories by counting the amount of words of each category
 * SingleCategorizer categorizes the String array to a specific category, (most common category)
 */
public class SingleCategorizer extends CategorizerBase{
	private LabelledTextList list;
	private String sortedCategories[][];
	private boolean removePunctuation;


	/**
	 * Constructor : initializes a SingleCategorizer object with a LabelledText list and a boolean
	 * representing whether the punctuation should be removed from the LabeleldTexts inside the LabelledTextList
	 * It takes the input and sorts it by category, removes the duplicates and punctuation
	 * @param A LabelledText list
	 * @param A boolean representing whether the punctuation should be removed from the LabeleldTexts inside the LabelledTextList
	 */
	public SingleCategorizer (LabelledTextList list,boolean removePunctuation ) {
		this.list = new LabelledTextList (list.getProblemSet());
		this.list.addAll(list);
		this.removePunctuation = removePunctuation;
        sortByCategory(list);
	}


	/* Returns an array of alphabetically sorted words of a specific category
	 * @return An array of alphabetically sorted words of a specific category
	 */ 
	public String [] getSentence (int index) {
		return this.sortedCategories[index];
	}


	/**
	 *Takes as input an index and returns the category at that index
	 *inside the LabelledTextList
	 *@param index of the category
	 *@return A String indicating the category at that index
	 */ 
	public String getCategory(int index) {
		return this.list.get(index).getCategory();
	}

	/**
	 *Takes as input a LabelledTextList list, sorts it
	 *Searches through that array for indexes of each category,
	 *Initializes array inside sortedCategories with specific size for each category
	 *Copies the data at those indexes into their category's specific array 
	 *Removes duplicates, punctuation(based on the removePunctuation boolean) 
	 *and upper case letters from those arrays
	 * @param LabelledText list  to be sorted 
	 * @return void
	 */ 
	private void sortByCategory(LabelledTextList labelledList){
		// initalizes the sorted arrays with corresponding sizes
		sortedCategories = new String [this.list.getProblemSet().getCategoryNames().length][];
		for (int i =0; i<this.list.getProblemSet().getCategoryNames().length; i++){
			for (int k =0; k<labelledList.size(); k++) {
				if ((getCategory(k)).equals(this.list.getProblemSet().getCategoryNames()[i])) {                   
					sortedCategories[i]=  ClassifierUtilities.addArrays(sortedCategories[i], labelledList.get(k).getText().split(" ") );
				}
			}
		}

		if (removePunctuation) {
			LabelledTextList listWithoutPunctuation = new LabelledTextList(list.getProblemSet());
			LabelledText [] nopunctuation =  new LabelledText[list.size()];

			for (int i =0; i< list.size(); i++ ) {  
				nopunctuation[i] = new LabelledText(list.get(i).getCategory(), list.get(i).getText().replaceAll("\\p{Punct}",""));
			}

			listWithoutPunctuation.addAll( Arrays.asList(nopunctuation));
			list = listWithoutPunctuation;
		}
		
		
		for (int i =0; i<sortedCategories.length; i++){
			sortedCategories[i] = ClassifierUtilities.getArrayasString(sortedCategories[i]).toLowerCase().split(" ");
			sortedCategories[i] = ClassifierUtilities.removeDuplicates (ClassifierUtilities.getArrayasString(sortedCategories[i]).split(" "));
			ClassifierUtilities.alphabeticalSort(sortedCategories[i]);

		}
	
	}


	@Override

	/**
	 *Takes as input a String which represents an entire document/text
	 *Determines its category based on the frequency of each category contained inside list.getProblemSet().getCategoryNames()
	 *Keeps track of a category's occurence with an int[] (isCategory[])
	 *Returns the category which occurs the most frequently, 
	 *@param A string representing an entire document/text
	 *@return A string representing the category that occurs the most frequently
	 */ 
	public String categorize(String text) {

		String [] document = text.split(" ");
		// only remove punctuation if boolean is true
		if (removePunctuation) {
			document = ClassifierUtilities.removePunctuationandCase(document);
		}
		//only remove uppercase, not punctuation
		else {
			for (int i =0; i<document.length; i++) {
				document[i].toLowerCase();
			}
		}

		document = ClassifierUtilities.removeDuplicates(document);
		ClassifierUtilities.alphabeticalSort(document);
		int [] isCategory = new int [list. getProblemSet().getCategoryNames().length];
		for (int i =0; i<sortedCategories.length; i++) {
			for (String word : sortedCategories[i]){
				if (ClassifierUtilities.find(word,document) == 0){
					//tracks category occurences
					isCategory[i]++;
				}

			}
		}

		for (int i =0; i<isCategory.length; i++) {
			if (ClassifierUtilities.findMax(isCategory) == i && isCategory[i] != 0 && isCategory[i] >= (document.length/2))
			{
				return list. getProblemSet() .getCategoryNames()[i];
			}
		}

		return null;
	}


}
