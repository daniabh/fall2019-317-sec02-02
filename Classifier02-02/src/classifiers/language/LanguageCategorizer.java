package classifiers.language;
import classifier.utilities.ClassifierUtilities;
import classifiers.language.Language;
import classifiers.shared.LabelledText;
import java.util.Arrays;

import classifier.interfaces.*;
/**
 * @author Dania Bouhmidi
 * LanguageCategorizer serves as a categorizer for String arrays that contain words in multiple languages
 * LanguageCategorizer enables us to determine the most frequent of those languages by counting the amount of words for each language
 * LanguageCategorizer categorizes the String array to a specific language, (most common language)
 */
public class LanguageCategorizer implements ICategorizer {
	private LabelledText [] texts;
	private String sortedLanguages[][];
	private Language language;
	/**
	 * Constructor : initializes a LanguageCategorizer object with a LabelledText array and a Language object
	 * It takes the input and sorts it by language, removes the duplicates and punctuation
	 * @param A LabelledText array 
	 * @param A Language object whose getCategoryNames() method returns a String array containing the valid categories
	 */
	public LanguageCategorizer (LabelledText [] texts,Language language) 
	{
		this.texts = new LabelledText[texts.length];
		ClassifierUtilities.copyArray(this.texts,texts);
		this.language = language;
		this.sortByLanguage(this.texts);

	}



	/* Returns an array of alphabetically sorted words in a specific language
	 * @return An array of alphabetically sorted words in a specific language
	 */ 
	public String [] getSentence (int index) {
		return this.sortedLanguages[index];
	}


	/**
	 *Takes as input an index and return the language at that index
	 *in the languages array
	 *@param index of the language
	 *@return A String indicating the language at that index
	 */ 
	public String getLanguage(int index) {
		return this.texts[index].getCategory();
	}

	/**
	 *Takes as input a LabelledText array, sorts it
	 *Searches through that array for indexes of those languages,
	 *Initializes array inside sortedLanguages with specific size for each language
	 *Copies the data at those indexes into their language's specific array 
	 *Removes duplicates, punctuation and upper case letters from those arrays
	 * @param LabelledText [] array to be sorted 
	 * @return void
	 */ 
	private void sortByLanguage(LabelledText [] array){
		Arrays.sort(array);

		// initalizes the sorted arrays with corresponding sizes
		sortedLanguages = new String [this.language.getCategoryNames().length][];
		for (int i =0; i<this.language.getCategoryNames().length; i++){
			for (int k =0; k<array.length; k++) {
				if ((getLanguage(k)).equals(this.language.getCategoryNames()[i])) {                   
					sortedLanguages[i]=  ClassifierUtilities.addArrays(sortedLanguages[i], array[k].getText().split(" ") );
				}
			}
		}
		for (int i =0; i<sortedLanguages.length; i++){
			sortedLanguages[i] = ClassifierUtilities.removePunctuationandCase(ClassifierUtilities.getArrayasString(sortedLanguages[i]).split(" "));
			sortedLanguages[i] = ClassifierUtilities.removeDuplicates (ClassifierUtilities.getArrayasString(sortedLanguages[i]).split(" "));
			ClassifierUtilities.alphabeticalSort(sortedLanguages[i]);

		}


	}




	/**
	 *Takes as input a String which represents an entire document/text
	 *Determines its language based on the frequency of each language contained inside language.getCategoryNames()
	 *Keeps track of a language's occurence with an int[] (isCategory[])
	 *Returns the language which occurs the most frequently, provided that more than half the
	 * words in text are considered that language
	 *@param A string representing an entire document/text
	 *@return A string representing the language that occurs the most frequently
	 */ 

	public String categorize (String text){

		String [] document = text.split(" ");

		document = ClassifierUtilities.removePunctuationandCase(document);
		document = ClassifierUtilities.removeDuplicates(document);
		ClassifierUtilities.alphabeticalSort(document);
		int [] isCategory = new int [language.getCategoryNames().length];
		for (int i =0; i<sortedLanguages.length; i++) {
			for (String word : sortedLanguages[i]){
				if (ClassifierUtilities.find(word,document) == 0){
					//tracks category occurences
					isCategory[i]++;
				}

			}
		}

		for (int i =0; i<isCategory.length; i++) {
			if (ClassifierUtilities.findMax(isCategory) == i && isCategory[i] != 0 )
			{
				return language.getCategoryNames()[i];
			}
		}

		return null;
	}


}

