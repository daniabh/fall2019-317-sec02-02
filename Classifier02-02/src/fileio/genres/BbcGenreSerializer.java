package fileio.genres;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;

import classifiers.shared.*;
import classifier.interfaces.*;

/**
 * 
 * @author Michelle Polonsky
 * This class is used to create a BbcGenreSerializer
 * and to load and save a directory with its sub folders
 *
 */
public class BbcGenreSerializer implements IDataSetSerializer {
	
	public String foldername; //is a given folder with sub folders 
	
	/**
	 * Constructor for BbcSerializer
	 * @param foldername is a path to a root folder for all the categories sub folders
	 */
	public BbcGenreSerializer(String foldername) {
		this.foldername = foldername;
	}
	
	/**
	 * This method takes the given foldername in the constructor and produces a LabelledTextList
	 */
	//take the folder given to the constructor and produce a LabelledTextList
	public LabelledTextList load() throws IOException { 
		
		SingleProblemSet categories =  new SingleProblemSet(getAllGenres(), true);
		
		LabelledTextList loadText = new LabelledTextList(categories);
		
		return loadText;
	}
	
	/**
	 * This method creates a new directory structure based on the LabelledTextList
	 * where the root folder is the foldername in the constructor
	 * and the sub folders are based of the LabelledTextList
	 */
	public void save(LabelledTextList labelledTextList) throws IOException {
		
		if(createFolder()) { //if folder already exists
			if(isFolderEmpty(this.foldername)) { //if folder is empty				
				fillRootFolder(labelledTextList, this.foldername); //fill the root folder with sub folders				
			}
			else {throw new IOException("Folder not empty");}
		}
		else { //the folder didn't already exist, but now it does
			fillRootFolder(labelledTextList, this.foldername); //fill the root folder with sub folders				
		}
	}
	
	/**
	 * Returns all the genres within a folder as a String[]
	 * @param foldername is a path to a directory
	 * @return a Genre with all the genres in the given folder
	 * @throws IOException is thrown if the given foldername is not an existing directory
	 */
	public String[] getAllGenres() throws IOException {
		
		File directory = new File(this.foldername); 
		
		FileFilter dirFilter = new FileFilter() { // stores all files that are directories
			public boolean accept(File file) {
				return file.isDirectory();
			}
		};
		
		File[] genresFile = directory.listFiles(dirFilter);
		
		String[] genresString = new String[genresFile.length];
	    for(int i=0; i<genresString.length; i++){
	      genresString[i] = genresFile[i].getName();
	    }	    
	    	    
	    //in the old version, this part returned a Genre based on genresString
	    //it is easier to use this method in more various ways if it returns a String[]
	    
	    return genresString;
	}/* snippet of code can be found on
	avajava.com/tutorials/lessons/how-do-i-use-a-filefilter-to-display-only-the-directories-within-a-directory.html*/
	
	/**
	 * Checks if a folder already exist, if not, it creates that folder
	 * @param foldername is the name of a folder to be created
	 * @return true if the folder already exists
	 */
	public boolean createFolder() throws IOException {
		
	    File folder = new File(this.foldername);
	    if(folder.mkdir()){ //folder exist?
	      System.out.println(foldername+" Folder Created");
	      return false;
	    }
	    else {return true;}
	}
	
	/**
	 * This methods check if a folder is empty and returns true if it is
	 * @param foldername is a folder
	 * @return true if the folder is empty
	 * @throws IOException if the folder does not exist
	 */
	public boolean isFolderEmpty(String foldername) throws IOException {
		
		File folder = new File(foldername);
		if(folder.list().length == 0){ //no files inside folder / folder empty
			return true;}
		else {return false;} //folder not empty
	}
	
	/**
	 * This method fills the root folder with the sub folders for the categories
	 * @param labelledTextList is a list of all the categories
	 * @param rootFolder is the root folder where the sub folders are being stored
	 * @throws IOException if the given genreFoldername is already an existing folder
	 */
	public void fillRootFolder(LabelledTextList labelledTextList, String rootFolder) throws IOException {
		
		for(int i=0; i<labelledTextList.getProblemSet().getCategoryNames().length; i++) {
			String genreFoldername = foldername+"\\"+labelledTextList.getProblemSet().getCategoryNames()[i];
			File genreFolder = new File(genreFoldername);
			if(genreFolder.mkdir()) {
				System.out.println(genreFoldername+" folder created");
			}
			else {throw new IOException("Folder "+genreFoldername+" already exists");}	
		}
	}

}
