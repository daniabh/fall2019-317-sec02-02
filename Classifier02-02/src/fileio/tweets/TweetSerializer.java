package fileio.tweets;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


import classifier.interfaces.IDataSetSerializer;
import classifiers.shared.LabelledText;
import classifiers.shared.LabelledTextList;
import classifiers.shared.SingleProblemSet;
/**
 * TweetSerializer implements IDataSetSerializer. 
 * The purpose of this class is to create a LabelledTextList based on a Tweeting dataset. 
 * This LabelledTextList object can then be used to create a MultiCategorizer.
 * @author Dania Bouhmidi
 *
 */
public class TweetSerializer  implements IDataSetSerializer {

	private String foldername;
	/**
	 * Constructor : Creates a TweetSerializer with the foldername given as input
	 * @param foldername : folder to load from or save to
	 * @throws IOException (if object's foldername does not exists)
	 */
	public TweetSerializer(String foldername) throws IOException{

		this.foldername = foldername;


	}
	/**
	 * Returns the foldername of the object
	 * @return foldername : folder that was used to create this TweetSerializer object
	 */
	public String getFolderName() {
		return this.foldername;
	}

	/**
	 * Creates a LabelledTextList from the tweets.tsv file contained inside the TweetSerializer 
	 * specified foldername
	 * @return LabelledTextList object
	 * @throws IOException (if the foldername of the object calling this method does not exist)
	 */
	@Override
	public LabelledTextList load() throws IOException {


		Path path= Paths.get(foldername+"//tweets.tsv");

		ArrayList <String> tweets= new ArrayList <String>();

		//reads all the lines from the tweets.tsv file

		tweets.addAll(   Files.readAllLines(path) );

		String [] emotionCategories = setEmotions(foldername+"//tweets.tsv");

		SingleProblemSet emotions = new SingleProblemSet(emotionCategories, true);
		LabelledTextList train = new LabelledTextList (emotions );

		for (int i =0; i<tweets.size(); i++) {
			String [] tweetRecord = tweets.get(i).split("\t");
			//contains one emotion
			if (tweetRecord.length == 3 ) {	
				train.add(new LabelledText(tweetRecord[2], tweetRecord[1] ));
			}
			//contains two emotions
			if (tweetRecord.length == 4) {
				train.add(new LabelledText(tweetRecord[2]+ ";"+tweetRecord[3], tweetRecord[1] ));
			}
			//contains three emotions
			if (tweetRecord.length == 5 && i != 0) { //ignore first line (contains titles)
				train.add(new LabelledText(tweetRecord[2]+ ";"+tweetRecord[3]+";"
						+tweetRecord[4], tweetRecord[1] ));

			}

		}

		return train;




	}
	/**
	 * 
	 * @param labelfile (file containing tweets and their emotions)
	 * @return String [] containing the tweet's emotions (without duplicates)
	 * @throws IOException (if the foldername of the object calling this method does not exist)
	 */
	private  String [] setEmotions(String labelfile) throws IOException {
		Path path1= Paths.get(labelfile);

		ArrayList <String> tweets = new ArrayList <String>();

		ArrayList <String> emotions = new ArrayList <String>();
		//stores all labels from input file(including duplicates)
		tweets.addAll(Files.readAllLines(path1));

		//ignore line 1 : (contains titles)
		for (int i =1; i<tweets.size(); i++) {
			String [] variousEmotions= tweets.get(i).split("\t");

			// if tweet record contains 1 emotion
			if (variousEmotions.length == 3    ) {
				emotions.add(variousEmotions[2]);

			}
			//if tweet record contains 2 emotions
			else if (variousEmotions.length == 4    ) {
				emotions.add(variousEmotions[2]);
				emotions.add(variousEmotions[3]);

			}
			//if tweet record contains 3 emotions
			else if (variousEmotions.length == 5  ) {

				emotions.add(variousEmotions[2]);
				emotions.add(variousEmotions[3]);
				emotions.add(variousEmotions[4]);
			}
		}



		//removes duplicate emotions
		ArrayList <String> emotionCategories = new ArrayList<String>();
		Set<String> set = new HashSet<>();
		set.addAll(emotions);

		emotionCategories.addAll(set);

		//initializes array with right size and sets values at each index
		String [] categories = new String[emotionCategories.size()];

		for (int i =0; i<emotionCategories.size(); i++) {
			categories[i] = emotionCategories.get(i);
		}


		return categories;

	}

	/**
	 * Takes as input a LabelledTextList object, parses the fields of the objects it contains and writes it into a file
	 * @return void
	 * @throws IOException (if the foldername of the object calling this method does not exist)
	 */
	@Override
	public void save(LabelledTextList labelledTextList) throws IOException {
	
		
		
		Path path = Paths.get(this.foldername);
		if (Files.isDirectory(path)) {
			throw new IOException ("Folder already exists");
		}
		else {
			File dir = new File(this.foldername);
			dir.mkdir();
		}
		
		
		ArrayList <String> listAsFile = new ArrayList<String>();
		Path outputPath= Paths.get(foldername+"//output.tsv");
		for (LabelledText element : labelledTextList) {
			if (element.getCategory().contains(";")) {
				String categories [] = element.getCategory().split(";");
				if (categories.length ==2) {
					listAsFile.add("\t"+element.getText() + "\t"+ categories[0]+ "\t"+categories[1]);
				}
				else if (categories.length == 3) {
					listAsFile.add("\t"+element.getText() + "\t"+ categories[0]+ "\t"+categories[1] + "\t"+ categories[2]);
				}
			}
			else {
				listAsFile.add("\t"+element.getText() + "\t"+ element.getCategory());
			}
		}
		
		Files.write(outputPath, listAsFile);  
	}

}
