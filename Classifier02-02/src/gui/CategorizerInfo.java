package gui;

import classifier.interfaces.ICategorizer;
import classifiers.shared.LabelledTextList;
import fileio.genres.BbcGenreSerializer;
import fileio.languages.WiliLanguageSerializer;
import fileio.tweets.TweetSerializer;
/**
 * A CategorizerInfo object holds a LabelledTextList as well as an ICategorizer, 
 * which should each be stored as private fields
 * The class provides a get method that allows us to retrieve each of these elements 
 * It allows these elements to be set from the constructor
 * It allows to set the categorizer type of the object (Emotion,Language,Genre)
 * @author Dania Bouhmidi
 */
public class CategorizerInfo {


	private LabelledTextList labelledList;
	private ICategorizer categorizer;
	private String categorizerType;

	/**
	 * Constructor : creates a CategorizerInfo and sets its labelledlist and categorizer field.
	 * @param labelledList 
	 * @param categorizer
	 */
	public CategorizerInfo(LabelledTextList labelledList, ICategorizer categorizer) {
		this.labelledList = labelledList;
		this.categorizer = categorizer;

	}

	/**
	 * Returns the labelledTextList this object contains
	 * @return the labelledTextList this object contains
	 */
	public LabelledTextList getLabelledList() {
		return labelledList;
	}

	/**
	 * Returns the Categorizer this object contains
	 * @return the Categorizer this object contains
	 */
	public ICategorizer getCategorizer() {
		return categorizer;
	}
	/**
	 * Returns the type of categorizer that is stored inside this object
	 * @return String typeOfCategorizer
	 */
	public String toString() {
		return this.categorizerType;
	}
	/**
	 * Sets the categorizer type of this object
	 * @param categorizertype
	 */
	public void  setCategorizer(String categorizertype){
		this.categorizerType = categorizertype;
	}




}
